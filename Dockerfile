FROM ubuntu:20.04

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

# SteamCMD license agreement
RUN echo steam steam/question select "I AGREE" | debconf-set-selections
RUN echo steam steam/license note '' | debconf-set-selections

# LinguxGSM dependencies
RUN dpkg --add-architecture i386 && apt update -y && \
    apt install -y \
        curl \
        wget \
        nano \
        file \
        tar \
        bzip2 \
        gzip \
        zip \
        bsdmainutils \
        iproute2 \
        cpio \
        python3 \
        util-linux \
        ca-certificates \
        binutils \
        bc \
        jq \
        tmux \
        netcat \
        lib32gcc1 \
        lib32stdc++6 \
        libsdl2-2.0-0:i386 \
        libtinfo5:i386 \
        steamcmd \
        && apt-get clean

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
