<h1 align="center">
  <br>
  <img src="https://github.com/GameServerManagers/LinuxGSM-Docs/raw/master/.gitbook/assets/linuxgsm_colour_logo_workmark_short_384.png" alt="LinuxGSM">
  <br>
  Docker
</h1>

[![LinuxGSM](https://img.shields.io/badge/-LinuxGSM-2b2b2b.svg?logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACsAAABACAYAAACDbo5ZAAAGD0lEQVR4AcWYa2wUVRvHz75c2vJ2disXoUBEVIioRAx%2BMIFA1IRwUS6KCCiIKBrvhEYiiGm5iBgk8QOGEgUUDYTttmVZWFr2UmwURS6iYAoCtmisElSsdHdbkT7%2BTzoNy%2BGc2dm54C%2F5Rdkz85x%2FZ86ZOXOYXYhIg3Phx%2FA4PAcv6f%2Btg5vhHJjP3EcZsgsshufJHH%2FAJbDztQ7aG35J1vgCXn%2BtgvrgEbLHYei9FmE3kDOUuh10OGwjZ7gEh7kZ9gNyllK3gnrg7%2BQs56DHjbCDyB0GuRF2ArnDeDfCziF3mO1G2FfJHRa4EXYpuUOxG2GXkzuUuBH2TXKH5W6ELSV3eM%2BNsOUk5yQcAY8p2uvgSPitor3MjbB1UOQXOFBvHwpbhPYWOExvL4SNUOQ7p4P2JTlzhOPmCu0vCu1PQpE2WOhk2MdJjk9y7HNwP1wEPUJbAcmZ6WTYrYolnifbxZBiibnFySHQCkUaLdY7C0V4%2FT5OhF1JcuIW6%2B0lOcvsBi2Ezao3j8Ov7Quwj52w60jNKIs1R5GatVaDDoEXSU4D7GSxbidYT3IuwjusFK0mNUVMwaZNNbl%2B%2F87hoVCom0HthaQmnG3QB0lNPcxlCvz%2B0GtlZTuorCxYYlA%2FDzbY%2FnrAgV3hCVLzCDMgEAi9wMMi9IIM%2FUwnNXWwi5mwRaSmmmXA798xq%2F3Khp420VeE1LyS6eQeBhttSXgzywCCToIUCOyYZvKLOWWwkdfd6ORVpGYJM0F5efDe9mEQHGty2BWTmhVGO4PNBmOoKzMBxmo%2FhD25dWvljSbD5hjMkb9gT9lJq63Mzi1bKkbjloc7xBWtwiRrDASCVRi%2FRxA8if%2BvrawMTzYI%2FACpeUv2KFFtDcUyTKilCHcis8G1Ga7wHpLzG8wVFs1K7jaeUKHZfIxmkj9%2FM4S9x9RGiMEudo0QbCVu9Xqn5PWEwLUk5%2FP0iaXac53I0kAH9Sau4m54lo%2FdTMfyekLYKQZ7ub34AbMMlmw5irC2VYTN0fuVMYMf8JHxJ7L7YYXAlSRnI2%2F8RtH4siTsDMz%2BZ5yS15OEXUxyDjGD1%2BvEq8MG16CDzY6JepKwT6geYbzxb0XjGMl7vwGSgzZIwo4nOa1M8Yr9Afb%2Fj8L21%2FsXaeaNZ2A6KTiEASph%2F0vF80cn4tokqu79fzfCUk2vfF4%2FFdFGEjGPHvg2yWrsDG%2F4%2BqpZB8jPOiXjWk0y5iXdM3XhKUvxBTA9UvHuvNPh%2Bzb%2BWj10z6nwuHWBbQFMvNCjKnn7qd1jS%2Fnx9VX3b6jC%2BbwOr5eMe3%2B83IcW4%2F3qgT%2BUTbA9srGaiHsn8AKiF6Ld%2FxF%2FOx6eeAJXabvK78MTTornNMd6XJTVR7%2Fj9bBjhFxV%2FMc1kJOAy2AeA%2FgrV4mFVDZFCqm8rEJ6qysCAWqK9iaztRJRbWXa4moFTELOav7jY%2FqAvoXp0Gc9NX7bIZl17%2FZl0rC1wRLeno0NvP%2B0CTcYNnS8wW6CtzNANaxzKqo9hRNOQ8rGo7tmSsMe2zWdt2frqVRcm5s2fofCgawDNHRNxnyfqgsYez4y4ADCjRM9H73hsNWafILTQdaFiSTj%2BQ%2FxA6yH7d%2BCmR8XbYr0a7VTF2N4siSsb5qdos3R6%2FjGxpULFfw7EfORnbrI9TAToX0sD40%2F2ylcGdh2Rdhg%2BSf2gka9jRTqK9%2BGwgHz7RTfVbHxirDhivdthYUvMRX8aYAD9lstvi%2B0aDL2DUZ0eCBUNNVG0IP65FLTWqPdiqdC0koHTVXe7iyNRG23QotBEy1RbTAzQyrmnYUT2rLsoA13JpcB4eVC2daBM1g2JGLekmwf5On7EJfngfZTNnVSce8bFjaTmQdvs%2FXmO%2FJVpoXdx3RSUe9O00Fj3nUdy0RLgRHiHXOPGW1h2uKZ07c9rG%2Bxyefp23pQeyBIEQoavYlakrG8fnrY5yHn2fbxXzAgw7m8bT5zkkSk4C7cpmOK21esB%2FXAQ5DzFfTo43%2BF%2FDzf0eZ4wZ3MDSjMcvD6nIeOahGgia%2FyU3HforTPkWkwnakdn0cI9jqfbPjD%2FuQLJr6644snlgX%2FApOMoyd1R5%2FxAAAAAElFTkSuQmCC)](https://linuxgsm.com)
[![Discord](https://discordapp.com/api/guilds/127498813903601664/widget.png?style=shield)](https://linuxgsm.com/discord)
[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://github.com/GameServerManagers/LinuxGSM/blob/master/LICENSE)

### docker-compose ([recommended](https://docs.linuxserver.io/general/docker-compose))

Compatible with docker-compose v3 schemas.

```
version: '3.7'

services:
  linuxgsm:
    image: registry.gitlab.com/counterstrikesource/linuxgsm:latest
    container_name: lgsm-docker
    network_mode: host
    stdin_open: true
    tty: true
    environment:
      INSTALL_ZIP_CMD: 'curl https://url/to/zip/file/zipfile.zip --output anynameisokay.zip'
      INSTALL_ONLY: "false"
      GAME_SERVER: "cssserver"
      GAME_SERVER_CMD: "start"
      AUTO_UPDATE: "false"
      PUID: 1000
      PGID: 1000
      TZ: "Europe/London"
    volumes:
      - /path/to/lgsm:/home/linuxgsm
    restart: always
```

### docker cli

```
docker run -d -it \
  --name=lgsm-docker \
  -e INSTALL_ZIP_CMD='curl https://url/to/zip/file/zipfile.zip --output anynameisokay.zip' \
  -e INSTALL_ONLY="false" \
  -e GAME_SERVER="cssserver" \
  -e GAME_SERVER_CMD="start" \
  -e AUTO_UPDATE="false" \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ="Europe/London" \
  -v /path/to/lgsm:/home/linuxgsm \
  --restart always \
  registry.gitlab.com/counterstrikesource/linuxgsm:latest
```

## Parameters

Container images are configured using parameters passed at runtime (such as those above). These parameters are separated by a colon and indicate `<external>:<internal>` respectively. For example, `-p 27020:27015` would expose port `27015` from inside the container to be accessible from the host's IP on port `27020` outside the container.

| Parameter | Function |
| :----: | --- |
| `-e INSTALL_ZIP_CMD=false` | Specify whether or not the container should only install a custom zip file at the root of the game server |
| `-e INSTALL_ONLY=false` | Specify whether or not the container should only install instead of running the server |
| `-e GAME_SERVER=cssserver` | Specify which server you would like to install (List of available options: https://linuxgsm.com/servers/) |
| `-e GAME_SERVER_CMD=start` | Specify which command should be first executed (List of available options: https://docs.linuxgsm.com/commands) |
| `-e AUTO_UPDATE=false` | Specify whether or not the game server should be updated on container startup |
| `-e PUID=1000` | for UserID - see below for explanation |
| `-e PGID=1000` | for GroupID - see below for explanation |
| `-e TZ=Europe/London` | Specify a timezone to use EG Europe/London |
| `-v /home/linuxgsm` | Contains all game server files. |

## User / Group Identifiers

When using volumes (`-v` flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user `PUID` and group `PGID`.

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

In this instance `PUID=1000` and `PGID=1000`, to find yours use `id user` as below:

```
  $ id username
    uid=1000(dockeruser) gid=1000(dockergroup) groups=1000(dockergroup)
```
