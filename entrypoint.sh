#!/bin/bash

PUID=${PUID:-911}
PGID=${PGID:-911}

echo "Creating default user, group and permissions"
addgroup --gid $PGID abc
adduser --uid $PUID --no-create-home --disabled-password --gecos "" --ingroup abc abc

usermod -G tty abc

echo "Creating base folders"
mkdir -p \
	/data

chown -R abc:abc /data/

# Set specific home folder because linuxgsm depends on it
echo "Setting user default home directory to /data"
usermod -d /data abc
cd /data

echo "Environment variables are being checked"
INSTALLED="false"
if [ "$GAME_SERVER" == "" ]; then
    echo "Environment variable GAME_SERVER must be set, choose one at this address https://linuxgsm.com/servers/"
    exit 84
fi

if [ "$INSTALL_ONLY" != "true" ] && [ "$INSTALL_ONLY" != "false" ]; then
    echo "Environment variable INSTALL_ONLY must be set to true or false"
    exit 84
fi

if [ "$INSTALL_ONLY" == "false" ] && [ "$GAME_SERVER_CMD" == "" ]; then
    echo "Environment variable GAME_SERVER_CMD must be set, choose one at this address https://docs.linuxgsm.com/commands"
fi

# Install the game server
if [ ! -e /data/linuxgsm.sh ]; then
    INSTALLED="true"

    echo "Initializing Linuxgsm User Script in New Volume"
    wget https://raw.githubusercontent.com/GameServerManagers/LinuxGSM/master/linuxgsm.sh -O /data/linuxgsm.sh    
    chmod +x /data/linuxgsm.sh && chown abc:abc /data/linuxgsm.sh

    echo "Installing $GAME_SERVER"
    runuser -l abc -c "/data/linuxgsm.sh $GAME_SERVER"
    runuser -l abc -c "/data/$GAME_SERVER auto-install"

    if [ "$INSTALL_ZIP_CMD" != "" ]; then
        echo "Installing custom files from .zip"
        eval "$INSTALL_ZIP_CMD"
        unzip -o *.zip -d /data/
        rm *.zip
    fi
    chown -R abc:abc /data
fi

# Install or run the server
if [ "$INSTALL_ONLY" == "true" ]; then
    if [ "$INSTALLED" == "false" ]; then
        echo "A server is already installed, nothing done"
    else
        echo "Server $GAME_SERVER has been successfully installed"
    fi
    exit 84
else
    if [ "$AUTO_UPDATE" == "true" ]; then
        # update LGSM
        runuser -l abc -c "/data/$GAME_SERVER update-lgsm"

        # update the game server
        runuser -l abc -c "/data/$GAME_SERVER update"
    fi

    echo "Executing game server command"
    runuser -l abc -c "export TERM=xterm"
    runuser -l abc -c "/data/$GAME_SERVER $GAME_SERVER_CMD"

    # if this command was a server start cmd
    # to get around LinuxGSM running everything in
    # tmux;
    # we attempt to attach to tmux to track the server
    # this keeps the container running
    # when invoked via docker run
    # but requires -it or at least -t
    echo "Attaching tmux output to /dev/null"
    if [ "$GAME_SERVER_CMD" == "start" ]; then
        runuser -l abc -c "tmux set -g status off && tmux attach 2> /dev/null"
    fi
fi
